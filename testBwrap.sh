#!/bin/bash

#get the VM name inputed
function main {
  echo $vm
  if [[ $vm == "" ]]
  then
    echo "You MUST enter the name of your VM" 
    echo "Ex. plague-bwrapLibvirt debian"
    echo 'To find the names of your VM; enter the command `virsh list --all`'
    exit
  else
    parseXML
  fi
}

function readXML {
  local IFS=\>
  read -d \< ENTITY CONTENT
}

function parseXML {
  while readXML
  do
    if [[ "$ENTITY" == *.qcow2* ]]
    then
      local qcow2Path=$(cut -d "'" -f2 <<< $ENTITY)
      if ! bubbleWrap $qcow2Path; then
        echo "An error was produced." 
      else
        virt-viewer $vm &> /dev/null &
      fi
      exit
    fi
  #   echo "$ENTITY => $CONTENT"
  done <<< $(virsh dumpxml $vm)
}

function bubbleWrap {
  bwrap \
    --ro-bind / / \
    --dev-bind /dev/ /dev/ \
    --tmpfs /var/log/ \
    --tmpfs /usr/src \
    --tmpfs /lib/modules \
    --tmpfs /usr/lib/modules \
    --tmpfs /run/ \
    --tmpfs /boot/ \
    --tmpfs /mnt/ \
    --tmpfs /tmp/ \
    --tmpfs /root/ \
    --tmpfs /home/ \
    --tmpfs /tmp/ \
    --bind $1 $1 \
    --bind /var/run/libvirt/ /var/run/libvirt/ \
    --unshare-user \
    --uid "$(id -u libvirt)" \
    --gid "$(id -g libvirt)" \
    --unshare-ipc \
    --unshare-pid \
    --share-net \
    --unshare-uts \
    --unshare-cgroup \
    --die-with-parent \
    --new-session \
    virsh start $vm
}

vm=$1
main

