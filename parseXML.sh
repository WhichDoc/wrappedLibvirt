#!/bin/bash

function readXML {
  local IFS=\>
  read -d \< ENTITY CONTENT
}

function parseXML {
  while readXML
  do
    if [[ "$ENTITY" == *.qcow2* ]]
    then
      echo $ENTITY $CONTENT
      cut -d "'" -f2 <<< $ENTITY
      exit
    fi
  #   echo "$ENTITY => $CONTENT"
  done < /etc/libvirt/qemu/voidlinux.xml
}

parseXML
